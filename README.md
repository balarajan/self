
bank-api
│   ├── pom.xml
├── src
│   ├── main
│   │   ├── java
│   │   │   └── com
│   │   │       └── bala
│   │   │           └── bank
│   │   │               ├── BankServiceApplication.java
│   │   │               ├── config
│   │   │               │   └── SwaggerConfig.java
│	│	│				├── constants
│	│	│				│	└── ErrorCode.java
│   │   │               ├── controller
│   │   │               │   ├── AccountController.java
│   │   │               │   └── TransactionController.java
│   │   │               ├── dto
│   │   │               │	└── ErrorResponse.java
│   │   │               ├── exception
│   │   │               │   ├── AccountNotExistException.java
│	│	│				│	├── ApplicationException.java
│	│	│				│	├── GlobalExceptionHandler.java
│	│	│				│	├── InsufficientBalanceException.java
│   │   │               │   └── TransactionNotExistException.java
│   │   │               ├── entity
│   │   │               │   ├── Account.java
│   │   │               │   └── Transaction.java
│   │   │               ├── repository
│   │   │               │   ├── AccountRepository.java
│   │   │               │   └── TransactionalRepository.java
│   │   │               └── service
│   │   │                   ├── AccountServiceI.java
│	│	│					├── TransactionalServiceI.java
│   │   │                   └── impl
│	│	│						├──	AccountServiceImpl.java
│   │   │                       └── TransactionalServiceImpl.java
│   │   └── resources
│   │       ├── application.properties
│   └── test
│       └── java
│           └── com
│               └── bala
│                   └── bank
│                       └── service
│                           └── impl
│								├──	AccountServiceImplTest.java
│                               └── TransactionalServiceImplTest.java
